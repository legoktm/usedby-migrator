// SPDX-License-Identifier: GPL-3.0-or-later
// (C) 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::{Result};
use mwbot::parsoid::{ImmutableWikicode, WikinodeIterator};
use mwbot::{Bot, Page, SaveOptions};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::BTreeMap;
use std::path::Path;

const NS_EXTENSION: i32 = 102;

#[derive(Default, Serialize, Deserialize)]
struct Site {
    id: String,
    name: String,
    extensions_cat: Option<String>,
    skins_cat: Option<String>,
    extensions: BTreeMap<String, bool>,
    skins: BTreeMap<String, bool>,
}

fn extract_args(html: ImmutableWikicode) -> Result<Vec<String>> {
    let html = html.into_mutable();
    for temp in html.filter_templates()? {
        if temp.name() == "Template:Used by" {
            let args: Vec<_> = temp.params().into_keys().collect();
            return Ok(args);
        }
    }
    Ok(vec![])
}

async fn blank(page: Page) -> Result<()> {
    let html = page.html().await?;
    let html = blank_args(html)?;
    page.save(
        html,
        &SaveOptions::summary(
            "Migrating {{[[Template:Used by|Used by]]}} data to [[Module:Used by/data.json]]",
        ),
    )
    .await?;
    Ok(())
}

fn blank_args(html: ImmutableWikicode) -> Result<ImmutableWikicode> {
    let html = html.into_mutable();
    for temp in html.filter_templates()? {
        if temp.name() == "Template:Used by" {
            for param in temp.params().keys() {
                temp.remove_param(param)?;
            }
        }
    }
    Ok(html.into_immutable())
}

async fn load_orig() -> Result<BTreeMap<String, Site>> {
let data: Vec<Site> = serde_json::from_str(&tokio::fs::read_to_string("data-orig2.json").await?)?;
    Ok(data.into_iter().map(|site| {
        (site.id.clone(), site)
    }).collect())
}

#[tokio::main]
async fn main() -> Result<()> {
    let bot = Bot::from_path(Path::new("mwbot.toml")).await?;
    let pagelist: Value = serde_json::from_str(&tokio::fs::read_to_string("pagelist.json").await?)?;
    let mut mapping: BTreeMap<String, Site> = load_orig().await?;
    for info in pagelist["query"]["embeddedin"].as_array().unwrap() {
        let page = bot.page(info["title"].as_str().unwrap())?;
        if page.title().contains('/') {
            continue;
        }
        println!("{}", page.title());
        let html = page.html().await?;
        let args = extract_args(html)?;
        if args.is_empty() {
            println!("error: found nothing");
            continue;
        }
        let root_text = page.as_title().dbkey().replace('_', " ");
        for arg in args {
            let site = mapping.entry(arg).or_default();
            if page.as_title().namespace() == NS_EXTENSION {
                site.extensions.insert(root_text.clone(), true);
            } else {
                site.skins.insert(root_text.clone(), true);
            }
        }
        blank(page).await?;
    }
    let data: Vec<_> = mapping
        .into_iter()
        .map(|(name, mut site)| {
            site.id = name;
            site
        })
        .collect();
    //tokio::fs::write("data.json", serde_json::to_string_pretty(&data)?).await?;
    Ok(())
}
